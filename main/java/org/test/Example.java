package org.test;

public class Example {

    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Example() {
    }

    public Example(String name) {
        this.name = name;
    }

    static  void  greeting (){
        System.out.println("Hello World!");
    }

}
